This repository contains analysis of convolution reverb applyed to several sound. The goal it to :
* understand the impact of noise in some of the Impulse response
* subjective evaluation of the mesured impulse response

Files & Folders
-----

* `src`: the jconvolver files used for convoltion of audio sample
* `analysis`: the website source generated and deployed by Gitlab CI, using mkdocs 
* `analysis/IRs`: the Impulse Responses
* `analysis/audio`: audio samples convolved with the IRs
* `.gitlab-ci.yml`: gitlab CI for website deployment
* `mkdocs.yml`: the website configuration

